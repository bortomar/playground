### module ###
module MyModule

  def for_each
    for i in 0...self.length        # must be ... (exclusive)
      yield(self[i])
    end
  end

  def cond_select
    result = []
    self.for_each { |x| result << x if yield(x) }
    result
  end

end
