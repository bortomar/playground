#!/usr/bin/ruby

### CONTROL FLOW ###

puts "Number 2 is even" if 2.even?
puts "Number 2 is not odd" unless 2.odd?


### LOOPS

max = 5

for i in 1...max      # .. is inclusive
  puts "for step #{i}"
end


until max < 1 do
  puts "until step #{max}"
  max -= 1
end

while max < 6 do
  puts "while step #{max}"
  max += 1
end


### METHODS ###

def greet name="John"
  puts "hello #{name}"
  name.length           # in last statement is return keyword optional
end

puts greet "Mario"


### ITERATORS AND BLOCKS ### 

5.times { |i| puts "hello #{i}" }


max.downto(1) do |i| 
  puts "downto step #{i}"
end

[1,2,3].each do |e|
  puts "Element: #{e}"
end

evens = [1,2,3,4].select { |e| e.even? } 
print "#{evens}\n"

