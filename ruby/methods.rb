#!/usr/bin/ruby

### methods ###

def greet name="John"
  puts "hello #{name}"
  name.length           # in last statement is return keyword optional
end

puts greet "Mario"
