#!/usr/bin/ruby

### classes ###

class Foo
  attr_writer :attr_w       # but not readable
  attr_reader :attr_r       # but not writable
  attr_accessor :attr_rw    # readle and writable
  
  def initialize(rw="default value")
    @attr_rw = rw
  end
  
  def who_am_i
    puts "I am: #{self}"    # #{self} calls to_s
  end
  def to_str
    "#{@attr_rw} (to_str)"
  end

  def to_s
    "#{@attr_rw} (to_s) / #{self.object_id}"
  end

  def w_attr=(value)        # alternative to attr_writer
    @w_attr = value
  end

  def w_attr
    @w_attr
  end

end

foo = Foo.new(5)

foo.who_am_i

puts "#{foo}"               # calls to_s
puts ""+foo                 # calls to_str

foo.w_attr = 'test'
puts foo.w_attr

class Bar < Foo
end

bar = Bar.new
bar.who_am_i

