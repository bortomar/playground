#!/usr/bin/ruby

### loops ###

max = 5

for i in 1...max      # .. is inclusive
  puts "for step #{i}"
end

until max < 1 do
  puts "until step #{max}"
  max -= 1
end

while max < 6 do
  puts "while step #{max}"
  max += 1
end


