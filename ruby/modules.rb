#!/usr/bin/ruby

### module usage ###
require './my_module.rb'


class Array
  include MyModule
end

evens = [1, 2, 3, 4, 5].cond_select { |x| x.even? }
print "#{evens}\n"
    
