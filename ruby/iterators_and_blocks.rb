#!/usr/bin/ruby

### iterators and blocks ### 

max = 5

5.times { |i| puts "hello #{i}" }


max.downto(1) do |i| 
  puts "downto step #{i}"
end

[1,2,3].each do |e|
  puts "Element: #{e}"
end

evens = [1,2,3,4].select { |e| e.even? } 
print "#{evens}\n"


### own block methods ###

def yield_method
  puts "This is yield method"
  yield("test string")
end 

yield_method { |s| puts "Print by yield: #{s}" }

### extending builtin class ###
class Fixnum
  def odd_times
    for i in 0...self        # instance of fixed number
      yield(i)
    end
  end
end

5.odd_times { |x| puts "#{x} is odd" if x.odd? } 
    
