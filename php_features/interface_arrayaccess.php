<?php
class FooArray implements ArrayAccess {
  private $list = [];
  
  public function offsetExists ($offset) {
    return isset($this->list[$offset]);
  }
  
  public function offsetGet ($offse) {
    return $this->offsetExists($offset) ? $this->list[$offset] : null;
  }
  
  public function offsetSet ($offset, $value) {
    if (is_null($offset)) {
        array_push($this->list, $value);
    } else {
      $this->list[$offset] = $value;
    }
  }
  
  public function offsetUnset ($offset) {
    if ($this->offsetExists($offset))
      unset($this->list[$offset]);
  }
}

$foo = new FooArray();
$foo[] = 'a';
$foo[] = 'b';
var_dump($foo);