<?php 

/*
 *  https://forum.root.cz/index.php?topic=17845.msg254769#msg254769 
 *
 * ... “self::” you refer to the current class (like “get_class” does)  and 
 * when you use “static::” you refer to the called class (like “get_called_class” does)
 * https://www.leaseweb.com/labs/2014/04/static-versus-self-php/
 * 
 * In non-static contexts, the called class will be the class of the object instance. 
 * Since $this-> will try to call private methods from the same scope, using static:: may 
 * give different results. Another difference is that static:: can only refer to static 
 * properties.
 * http://php.net/manual/en/language.oop5.late-static-bindings.php
 */
 
header("Content-Type: text/plain; charset=utf-8");

class A {
	private function who() {
		debug_print_backtrace();
	}
	
	public function runFn($callable) {
		debug_print_backtrace();
    
    if ($callable instanceof Closure) {       // Closer extends stdClass
      $callable();
    } else {
      call_user_func([$callable, 'callWho']);
    }
    
	}
	
	public function testA() {
		debug_print_backtrace();
		
    $this->who();
	}
	
	public function testB() {
		debug_print_backtrace();
		
    B::who();          
    // same as 
		// static::who();
		// self::who();
	}	
}

class B extends A {  

	public function testC() {
		debug_print_backtrace();
		
    B::who();		
    // same as 
		// static::who();
		// self::who();
	}
  
  /* Overrides A::who() so when uncommented, testC() would call it.
   *
   * Changing modificator to private would result in following error:
   * Call to private method B::who() from context 'A' at ..
  
  protected function who() {
		debug_print_backtrace();
	}
	
   */
}

$b = new B();

try {   
    #0  A->who() called at ...
    #1  A->testA() called at ...
    $b->testA();
} catch (Error $e) {
    echo $e->getMessage().' at '.$e->getLine().PHP_EOL;
}

try {
    #0  A->who() called at ...
    #1  A->testA() called at ...
    $b->testB();
} catch (Error $e) {
    echo $e->getMessage().' at '.$e->getLine().PHP_EOL;
}

try {
    #1  B->testC() called at ...
    $b->testC();
} catch (Error $e) {
    # Call to private method A::who() from context 'B' at ...
    echo $e->getMessage().' at '.$e->getLine().PHP_EOL;
}

try {
    #0  A->runFn(Closure Object ()) called at ...        
    $b->runFn(function() { 
        B::who();
        // same as
        // static::who();
        // self::who();
    });
} catch (Error $e) {
    # Call to private method A::who() from context ' ' at ...        
    echo $e->getMessage().' at '.$e->getLine().PHP_EOL;
}

try {    
    #0  A->runFn(class@anonymous Object ()) called at ...
    $b->runFn(new class { 
        public function callWho() { 
          B::who();
          /* the following would result in an error
           * static::who();
           * self::who();
           */
        }
    });
} catch (Error $e) {
    # Call to private method A::who() from context 'class@anonymous' at ...
    echo $e->getMessage().' at '.$e->getLine().PHP_EOL;
}