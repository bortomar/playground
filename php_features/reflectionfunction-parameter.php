 

<?php    
    
    class Whatever {
      
      public function __toString() {
        return 'string representation of Whatever';
      }
    }
    
    $args = [];
    $types = [
      'int' => 1,
      'Whatever' => new Whatever()
    ];
    
    function test(int $i, Whatever $w) {
      echo $w;
      echo $i;
    }
    
    $fn = new ReflectionFunction('test');
    
    foreach ($fn->getParameters() as $pObj) {
      $arg = new ReflectionParameter('test', $pObj->name);
      array_push($args, $types[(string)$arg->getType()]);
    }
    
    
    call_user_func_array('test', $args);
?>


    
    
    