<?php

$results = array();

for ($i = 2; $i >= 0; $i--) {
	try {									
		$results[] = intdiv(1, $i);		    // expression 1 / $i displays only warning
	} catch (DivisionByZeroError $e) {  // same as wrapping loop by try-catch block
		$results[] = $e->getMessage();
	}
}	

print_r($results);