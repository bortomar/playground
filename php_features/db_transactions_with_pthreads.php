<?php

// PHP Pthreads extension needed

$foo = new class extends Thread {
  
  public function run() {
      $db = new PDO("mysql:dbname=bb-ms360;host=localhost", "root", "root");      
      
      //while (true) {
        //$db->query("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE");
        $db->beginTransaction();
        
        $x = $db->query("SELECT MAX(poradove_cislo) as poradove_cislo FROM cislo_jednaci FOR UPDATE")->fetch();
        print("foo: selected\n");
        
        //$db->query("UPDATE cislo_jednaci SET poradove_cislo = ".($x['poradove_cislo'] + 1)." WHERE id = 8");
        //print("foo: updated\n");
        
        $db->query("INSERT INTO cislo_jednaci (podaci_denik, rok, poradove_cislo) VALUES ('denik', 2019, ".($x['poradove_cislo'] + 1).")");
        print("foo: inserted\n");
        
        //sleep(5);
        
        $db->commit();
      //}
  }
};


$bar = new class extends Thread {
  
  public function run() { 
      $db = new PDO("mysql:dbname=bb-ms360;host=localhost", "root", "root");
      
      //while (true) {
        //$db->query("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE");
        $db->beginTransaction();
        
        $x = $db->query("SELECT MAX(poradove_cislo) as poradove_cislo FROM cislo_jednaci FOR UPDATE")->fetch();
        print("bar: selected\n");
        
        //$db->query("UPDATE cislo_jednaci SET poradove_cislo = ".($x['poradove_cislo'] + 1)." WHERE id = 8");        
        //print("bar: updated\n");
        
        $db->query("INSERT INTO cislo_jednaci (podaci_denik, rok, poradove_cislo) VALUES ('denik', 2019, ".($x['poradove_cislo'] + 1).")");
        print("bar: inserted\n");
        
        $db->commit();
        
      //}
  }
};

$foo->start();
//sleep(1);
$bar->start();