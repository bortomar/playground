<?php

  // http://php.net/references.return
  
  $demoArray = ['a', 'b'];
  
  function returnValue() {
    global $demoArray;        // need to access extern variable
    return $demoArray;
  }
  
  $val = returnValue();
  array_pop($val);
  
  var_dump($demoArray);
 
   
  function &returnRef() {
    global $demoArray;
    static $staticDemoArray = ['c','d'];
    
    if (count($staticDemoArray) < 2) {
      var_dump($staticDemoArray);
      
      return $demoArray;
    
    }
    
    return $staticDemoArray;
  }
   
  $val = returnRef();     // only copy is returned
  array_pop($val);
  
  var_dump($demoArray);
  
  $ref = &returnRef();    // get reference to static array stored on the heap
  array_pop($ref);
  
  $ref = &returnRef();    // get reference to global array
  array_pop($ref);
  
  var_dump($demoArray);
  
  