<?php

// 1. Warning Error
// 2. Notice Error
// 3. Parse Error
// 4. Fatal Error

set_error_handler(function($errno, $errstr) {
    var_dump('error handling: #' . $errno . ': ' . $errstr);
    
    // posibility of throwing own exception
    // throw new Exception('Handler: ' . $errstr);
}, E_NOTICE);

try {
    // warning: 
    1 / 0;
    
    // notice: 
    $f = []; $f[1];
    
    restore_error_handler();
    
    // notice: 
    $f[2];
    
    // fatal error "uncatchable" by error handler: 
    // new XXX();
    
    // parse error "uncatchable" by error handler: 
    // Xew DateTime();

} catch (\Exception $e) {
    var_dump($e->getMessage());
}