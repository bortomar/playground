<?php
    
	if (isset($_GET['get-file'])) {
		
		// Pro testování předpokládáme, že jde o obrázek
		header('Content-Type: image/jpeg');
		
		// 'attachment' přinutí browser otevřít dialogové okno stažení
		//header('Content-Disposition: attachment; filename="'.urldecode($_GET['get-file']).'"');
		
		// urldecode protože může obsahovat diakritiku apod.
		//iconv("UTF-8", "cp1250", ...) protože Windows
		echo file_get_contents(iconv("UTF-8", "cp1250", urldecode($_GET['get-file'])));			
	
	} else {
		file_put_contents(iconv("UTF-8", "cp1250", urldecode(getallheaders()['X-Nazev-Souboru'])), file_get_contents('php://input'));
		
		echo '<a href="'.basename(__FILE__).'?get-file='.urlencode(getallheaders()['X-Nazev-Souboru']).'">Zobrazit</a><br />';
		//echo '<img src="'.getallheaders()['X-Nazev-Souboru'].'" />';
	}
	
	
	
?>	
