<?php

  header("Access-Control-Allow-Origin: *");
  
  // Je-li request HEAD, pošleme si obsah v hlavičkách
  if ($_SERVER['REQUEST_METHOD'] == "HEAD") { 
	header("X-Obsah-GET: ".odstran_radky_mezery($_GET));		
	header("X-Hlavicky-Pozadavku: ".odstran_radky_mezery(getallheaders()));
	
	// Prazdné - neni v požadavku ani odeslano
	header("X-Obsah-POST: ".odstran_radky_mezery($_POST));
	header("X-Raw-Request-Data: ".file_get_contents("php://input"));
  }
  
  echo "Raw data (php://input):\n".file_get_contents("php://input")."\n";
  echo "\n\n";
  echo "\$_GET:\n".print_r($_GET, true)."\n";
  echo "\$_POST:\n".print_r($_POST, true)."\n";
  echo "HEAD (request):\n".print_r(getallheaders(), true)."\n";
  
  function odstran_radky_mezery($s) {
	return preg_replace("/[\n ]/", "", print_r($s, true));
  }
  
?>