<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
    
    </head>
    
<body>
    <?php 
    /*****************************************************/
	/*** readAsDataURL() - data:image/jpeg;base64, ... ***/
	/*****************************************************/
	// načtení
	$data = file_get_contents($_FILES['soubor']['tmp_name']);
    
	// zobrazení
    //echo '<img src="'.$data.'" />';
	
	// uložení - potřebujeme získat data bez informace o jejich typu a kódování - data:image/jpeg;base64,
    //$tmp = explode(',', $data);
	
	// Název nejprve dekódujeme z url interpretace a ve Windows převedeme do správného kódování.
	$nazev = !$_POST['nazev'] ? $_FILES['soubor']['name'] : iconv("UTF-8", "cp1250", urldecode($_POST['nazev'])).".jpg";
	
	// Dékodujeme zpět do binární podoby.
	//file_put_contents($nazev , base64_decode($tmp[1]));
	
	/*****************************************************/
	/*** readAsArrayBuffer()						   ***/
	/*** - pole byte Uint8Array                        ***/
	/*****************************************************/
		
	// $data načítáme stejně
	
	// zobrazení - doplníme chybějící info o typu a kódování a navíc musíme binární data enkódovat
    echo '<img src="data:image/jpeg;base64, '.base64_encode($data).'" />';
	
	// uložení - nepotřebujeme oddělovat údaj o typu a kódování ani dekódovat do binárky, $nazev generujeme stejně
	file_put_contents($nazev , $data);
	
	    
	/**********************************************************************/
	/*** btoa(readAsBinaryString())										***/
	/*** - rozdíl pouze v chybějící předponě "data:image/jpeg;base64, " ***/
	/**********************************************************************/
		
	// $data načítáme stejně
	
	// zobrazení - doplníme chybějící info o typu a kódování
	// echo '<img src="data:image/jpeg;base64, '.$data.'" />';
	
	// uložení - nepotřebujeme oddělovat údaj o typu a kódování, $nazev generujeme stejně
	//file_put_contents($nazev, base64_decode($data));
	
	
	/*** Alternativně můžeme zobrazit až po uložení souboru. ***/
	//echo '<img src="'.urldecode($_POST['nazev']).'.jpg" />'; 	
	
	
	
	// http://stackoverflow.com/questions/20556773/php-display-image-blob-from-mysql
?>
</body>
</html>