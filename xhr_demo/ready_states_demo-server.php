<?php
	// je-li příklad spuštěn z průzkumníka (protokol file:///... ),
	// konzole hlasí: "Žádost Cross-Origin zablokována" - problém řeší 
	// odeslání následující HTTP hlavičky
	header("Access-Control-Allow-Origin: *");
	
	function foo() {
		$id = isset($_POST['id']) ? $_POST['id'] : 0;
		return "Tělo požadavku: (id = " . $id . ")";
	}
	
	echo foo();
?>