const namedExport = require('./namedExport.js')
const { hello, hi } = require('./namedExport.js')
const wrongDefaultExport = require('./wrongDefaultExport.js')
const defaultExport = require('./defaultExport.js')

function exportedContent(exported) {
        console.log('=========')
	console.log('exported property content:')
        console.log(exported)
}
function tryExecute(callback) {
        console.log('=========')
	console.log('Trying to call...')
        console.log('.........')
	callback()

}

function runIt(callback, exported = null) {
	try {
		exportedContent(exported || callback)
		tryExecute(callback)
	} catch (error) {
		console.error(error)
	} finally {
        	console.log('...')
	}
}

runIt(hello, namedExport)
runIt(hi, namedExport)
runIt(wrongDefaultExport)
runIt(defaultExport)

