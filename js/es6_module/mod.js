// export default const foo... SyntaxError https://stackoverflow.com/questions/36261225/why-is-export-default-const-invalid

export default "foo"

export const bar = "bar"

export const fooFun = () => ({ msg: "fooFun" })     // round parenthesis are required for object literal



