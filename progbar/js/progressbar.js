function propResize(el, w, h) {
	el.height(el.width() * h / w);
}


jQuery(document).ready(function() {

	// === new progressbar ===
	var dogs = jQuery(".dog");

	dogs.each(function() {
		
		var dog = jQuery(this);
		
		dog.prepend('<div class="dogprogress"></div>');
		
		var progress = dog.find(".dogprogress");
		var dogimg = dog.find("img");
		var p = (dog.data("vybrano") / dog.data("cilova")).toFixed(2);
			
		dog.height(dogimg.height());			
		
		if (dog.hasClass("animate")) {
			dog.waypoint(function() {
				if  (!progress.hasClass("filled")) {
					progress.addClass("filled");

					progress.css({
						'height': '0px'
					}).animate({
						'height': (dogimg.height() * p) + 'px'
					}, 2000);
				}			
			}, {
				offset: '50%'
			});
		} else {
			progress.height(dogimg.height() * p);
		}
		
		jQuery(window).resize(function() {
			dog.height(dogimg.height());
			progress.height(dogimg.height() * p);
		});
		
	});
	// ===================
});