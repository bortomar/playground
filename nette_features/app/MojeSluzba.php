<?php
// Nette - jak zapisovat služby (extension / neon) 
// https://f3l1x.io/blog/2015/10/17/nette-jak-zapisovat-sluzby/  
namespace App\Services;

class MojeSluzba {
    
    /** @var \Nette\DI\Container @inject */
    public $injectedCont;
    
    public $name;
    
    
    public function __construct($title) {
        $this->name = $title;
    }
    
    public function injectTest(\Nette\DI\Container $cont) {
        //$this->injectedCont = $cont;
    }
    
    public function testInjection() {      
        dumpe($this->injectedCont);
    }
}
