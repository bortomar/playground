<?php
 // Píšeme rozšíření (compileru) pro Nette Framework 
 // https://filip-prochazka.com/blog/piseme-rozsireni-compileru-pro-nette-framework
namespace App\Services;

class MojeExtension extends \Nette\DI\CompilerExtension {
  
    private $argSluzby;
    // protected $config; zděděno, netřeba uvádět
    
    public function __construct($parametrSluzby) {
        $this->argSluzby = $parametrSluzby;
    }
    
    public function loadConfiguration() {
        $builder = $this->getContainerBuilder();
        
        /* Do $config property objektu jsou kompilátorem dodány konfigurační argumenty 
         * přidané prostřednictvím Configurator::addConfig(['sluzba => [...]])
         * Alternativně lze konfiguraci získat pomocí CompilerExtension::getConfig()
         */
        $cfg = $this->config ;
        
        /* Přidáme prefix služby "rozsireni.sluzba", identifikující rozšíření, které
         * službu registrovalo, lze uvést pouze název "sluzba"
         */
        $builder->addDefinition($this->prefix("sluzba"))
              ->setFactory(\App\Services\MojeSluzba::class, [$cfg['title']])
              //->setInject(true) deprecated
              ->addTag(\Nette\DI\Extensions\InjectExtension::TAG_INJECT) // povoluje použití @inject anotace a inject* metod
              ->setAutowired(true);   // umožňuje zíkání instance služby pomocí DI
    }
}
