<?php
  
namespace App\Presenters;

use Nette; // abychom nemuseli psát uvozující \Nett... (jsme v App\Presenters)


final class AplikacePresenter extends Nette\Application\UI\Presenter {
      
      private $sluzba;
      
      public function __construct(Nette\DI\Container $cont, \App\Services\MojeSluzba $sluzba, Nette\Database\Connection $conn) {
          
          \Tracy\Debugger::barDump('__construct');            
          
          //$this->sluzba = $cont->getService('sluzba.sluzba');
          $this->sluzba = $sluzba;     // autowiring
      }
      
      /*
       * https://doc.nette.org/cs/2.4/presenters#toc-zivotni-cyklus-presenteru
       * https://doc.nette.org/cs/2.4/presenters#toc-odeslani-odpovedi
       */
      protected function startup() {
          parent::startup();
          \Tracy\Debugger::barDump('startup');            
          
          /*
           * Připojení komponenty (Control) do presenteru:
           * https://doc.nette.org/cs/2.4/presenters#toc-presenter-a-komponenty
           * https://doc.nette.org/cs/2.4/components
           */
           $this->addComponent(new \App\MenuControl(), 'menuControl');
           // alternativně: $this['menuControl'] = new \App\MenuControl();

           $f = new Nette\Application\UI\Form();
           $f->addText('xxx', 'Xxx'); 
           $f->addSubmit('odeslat', 'Odeslat');
           $f->onSuccess[] = function()  { $this->flashMessage("Form sent"); };
           $f->onSuccess[] = [$this, 'submitTest'];
           $this->addComponent($f, 'formular');            
      }
      
      public function submitTest($f) {
        //dumpe($this->getHttpRequest()->getRawBody());
      }
      
      /*
       * Další možností, jak vytvářet komponenty je přes tovární metodu:
       * https://doc.nette.org/cs/2.4/presenters#toc-tovarny-na-komponenty
       */
      protected function createComponentFootControl() {
          return new \App\FootControl();
      }
      
      /* 
       * Překrytí Presenter::formatTemplateFiles
       * https://api.nette.org/2.4/source-Application.UI.Presenter.php.html#526
       */
      public function formatTemplateFiles(): array {
        
        list(, $presenter) = \Nette\Application\Helpers::splitName($this->getName());
        $dir = dirname($this->getReflection()->getFileName());
        
        /* přidáme do pole formátů názvu rodiče vlastní název souboru šablony */
        return array_merge(
          parent::formatTemplateFiles(),
          ["$dir/$presenter.$this->view.latte"]  
        );      
      }
      
      /*
       * ?action=asnyc&id=[hodnota] resp. /async/[hodnota]
       * - je-li de implementována, následuje stejnojmenná render metoda,
       *   jinak se vyhledá příslušný (stejnojmenný) soubor se šablonou
       */      
      public function actionActn($id) {   
          
          \Tracy\Debugger::barDump('actionActn');            
      
          $this->payload->actionData = $id;
      }
      
      public function injectCokoliv(Nette\Http\Request $req, Nette\DI\Container $cont) {
        /* Prostřednictvím parametrů metod s prefixem inject* lze na základě autowiringu
         * získat služby registrované v DI kontejneru
         */
      }
      
      /*
       * ?do=asnyc&arg=[hodnota]
       * - následuje render metoda aktuálního view (refresh aktuální stránky)
       */
      public function handleHndl($arg) { 
          
          \Tracy\Debugger::barDump('handleHndl');
          
          if ($this->isAjax()) {            
              // $this->sendResponse(new  Nette\Application\Responses\JsonResponse(['klic' => 'hodnota']));       
              $this->payload->handleData = $arg;
              $this->sendPayload();
          } 
      }
         
      public function actionDefault() { 
          
          \Tracy\Debugger::barDump('actionDefault');                     
          
      }
      
      public function renderDefault() { 
          
          \Tracy\Debugger::barDump('renderDefault');        
             
          $this->template->name = $this->sluzba->name;                    
          
      }
}
