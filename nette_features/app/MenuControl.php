<?php

namespace App;

class MenuControl extends \Nette\Application\UI\Control {
     
     
      public function injectCont(\Nette\DI\Container $cont) {
        $this->cont =  $cont;
      }
      
      public function render($msg) {
        $this->template->msg = $msg;        
        $this->template->render(__DIR__.'/MenuControl.latte');        
      }
}