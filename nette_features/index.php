<?php    
    
    require __DIR__ . '/vendor/autoload.php';
    
    $configurator = new Nette\Configurator;
    
    /* před registrací RobotLoaderu je vhodné nastavit tmp adresář 
     * (lze nastavit i přes metodu RoboLoader::setTempDirectory)
     */
    $configurator->setTempDirectory(__DIR__ . '/tmp');
    
    
    
    $roboLoader = $configurator->createRobotLoader()
        ->addDirectory(__DIR__ . '/app')
        ->register(); 
    
    
    
    $configurator->enableTracy();
    
    
    
    /* předaní konfigurace pro účely vytvoření Nette\Database\Connection prostřednictvím 
     * standardního rozšíření DatabaseExtension (viz Configurator::defaultExtensions)
     * v metodě DatabaseExtension::loadConfiguration() 
     * alternativa k $configurator->addConfig(__DIR__ . '/config.local.neon');
     */
    $configurator->addConfig([
        'database' => [
            'dsn' => 'mysql:host=127.0.0.1;dbname=test',
            'user' => 'root',
            'password' => 'root'
        ]
    ]);
    
    $configurator->addConfig([
        'forms' => [
            'messages' => [
              'EMAIL' => 'test'
            ]
        ]
    ]);
    
    
    
    /* Alternativní způsob (obvykle confgi.neon - sekce extensions) přidání 
     * valstního rozšíření (potomek CompilerExtension) zařazením mezi 
     * standardní rozšíení Configurator::defaultExtensions
     */
    $configurator->defaultExtensions['rozsireni'] = [
        \App\Services\MojeExtension::class,
        ['%parametrSluzby%']
    ];
    
    /* parametr konstruktoru vlastního rozšíření */
    $configurator->addParameters([
        'parametrSluzby' => 'FOO'
    ]);
    
    /* a do konfigurace můžeme přidat další parametry služby */
    $configurator->addConfig([
        'rozsireni' => [
            'title' => 'Moje rozšíření implementující CompilerExtension'
        ]
    ]);
    
    /* Obvykle se pro registraci služby použivá config.neon - sekce services,   
     * každá služba z konfiguračního souboru je pro účely kompilace DI kontejneru 
     * přidána do konfigurace jako Statement objekt 
     *
        $configurator->addConfig([
          'services' => [
              'sluzba' => new Nette\DI\Statement(
                  App\Services\MojeSluzba::class,
                  []
              )
          ]
        ]);
      
      */
    
    /* Služby přidané prostřednictvím 
     * $configurator->addServices(['MojeSluzba' => new App\Services\MojeSluzba()]);
     * nebo $container->addService('MojeSluzba', new App\Services\MojeSluzba());
     * (prakticky totžné viz https://forum.nette.org/cs/31311-registrace-sluzby-neon-vs-addservice#p199923)
     * jsou v obou případech přidány teprve po vygenerování (kompilaci) Containeru, 
     * proto nefunguje autowiring.
     */
     
    //dumpe($configurator);
    
    /* vytvoření Containeru
     *    
      Configurator::createContainer -> { 
          Configurator::loadContainer ->  { 
              DI\ContaierLoader::load -> { 
                  DI\ContaierLoader::loadFile -> { 
                      DI\ContaierLoader::generate -> {
                          Configurator::generateContainer -> {
                              DI\Compiler::loadConfig -> {
                                  DI\Config\Loader::load -> {
                                      DI\Config\Loader::getAdapter -> {
                                          DI\Config\Adapater::load -> { 
                                              DI\Config\Adapater::process(Neon\Neon::decode -> {
                                                  Neon\Decoder::decode -> {
                                                      Neon\Decoder::parse 
                                                  } 
                                              })     
                                          } 
                                      } 
                                  }
                              } 
                          }                        
                          DI\Compiler::compile -> { 
                              DI\Compiler::generateCode -> {
                                  DI\ContainerBuilder::resolve -> 
                                  DI\ContainerBuilder::complete -> 
                                  DI\PhpGenerator(DI\ContainerBuilder) ->
                                  DI\PhpGenerator::generate
                              } 
                          }
                      } 
                  } 
              } 
          } 
      }
     */
    $container = $configurator->createContainer();
    
    
    
    /* mapování http requestu - alternativa k $configurator->addConfig(__DIR__ . '/config.local.neon');
     * nebo 
     
        $configurator->addConfig([
            'application' => [
              'mapping' => ['*' => 'App\Presenters\*Presenter']
            ]
        ]);
        
     */
    $container->getByType(Nette\Application\IPresenterFactory::class)
        ->setMapping(['*' => 'App\Presenters\*Presenter']);
       
    
    
    
    /* přidání instance routeru */
    //$container->addService('router', new Nette\Application\Routers\SimpleRouter('Aplikace:default'));    
    
    $router = $container->getByType(Nette\Application\IRouter::class);    
    $router[] = new Nette\Application\Routers\Route('<presenter>[/<action>][/<id>]', 'Aplikace:default');
    // $router[] = new Nette\Application\Routers\SimpleRouter('Aplikace:default');
   

   
    $container->getByType(Nette\Application\Application::class)->run();
   
    //\Tracy\Debugger::dump($configurator);   
    //\Tracy\Debugger::dump($container);
    
    // ===============================================================================
    
    
    
    // Nette\Forms\Form extends
    // Nette\Forms\Container extends 
    // Nette\ComponentModel\Container extends 
    // Nette\ComponentModel\Component 
/*    
    class Formular extends Nette\Forms\Form {
        public function createComponentKomponenta() {
            return new class extends Nette\Forms\Container{};
        }
    }
    $frm = new Formular();
    $frm->getComponent('komponenta');
    
    \Tracy\Debugger::dump($frm);   
    
*/    
    
    
    
    
    